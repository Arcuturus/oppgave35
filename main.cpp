//   Fil:  FRODEH \ OO \ EKSEMPEL \ EKS_25.CPP

//   Programeksempel nr.25 for forelesning i  C++ -programmering.


//  Programmet leser inn data om billetter til 10 langrennsarrangementer
//  fra filen "BIL_OLXX.DTA". Det leser ogs† inn bestillingene for 200
//  kunder (fra "KUN_OLXX.DTA"). Ut fra dette s† fordeler programmet
//  billettene og skriver resultatet bin‘rt til filene "*.RES".

#ifdef _MSC_VER
#define _CRT_SECURE_NO_WARNINGS
#endif

//  INCLUDE:
#include <fstream>                     //  ifstream, ofstream
#include <iostream>                    //  cout
#include <cstring>                     //  strcpy
#include <cstdlib>                     //  (s)rand
#include <iomanip>
using namespace std;

//  CONST:
const int  ANTARR     =  11;           //  Antall arrangement (1-10 stk).
const int  ANTKUNDER  = 201;           //  Antall kunder (1-200 stk).
const int  NVNLEN     =  20;           //  Kundenavns lengde.
const int  ARRLEN     =  35;           //  Arrangementnavns lengde.

enum Oppdater {  Bestilt,  Faatt,  Kopier  };


//  DEKLARASJON AV FUNKSJONER:
void skrivBilletterTilFil();
void skrivKunderTilFil();
void lesKunder();
void lesArrangementer();
void skapFaktura();
void printStars();

//  KLASSER:
class Kunde  {
private:
    char navn[NVNLEN];                 //  Kundens navn.
    int  antBestilt[ANTARR];           //  Antall bestilte pr. arrangement.
    int  antFaatt[ANTARR];             //  Antall f†tt pr. arrangement.
    
public:
    Kunde();
    void oppdaterNavn(char nvn[]);
    void oppdaterBestiltFaatt(int nr, Oppdater oppdat, int ant = 0);
    int  antOnsket(int nr);
    bool faattBillettAllerede(int nr);
    //  Ekstra funksjon for † se hva som skjer ved trekkingen:
    void skriv(ofstream &f) {
        f << "\n\n\tTil:\t\t" << navn << "\n\n";
    }
    int antFatt(int pa) {
        return antFaatt[pa];
    }
    char * hentNavn() {
        return navn;
    }
};


class Arrangement  {
private:
    char tittel[ARRLEN];               //  Arrangementets tittel/navn.
    int  pris;                         //  Pris pr. billett.
    int  antPlasser;                   //  Antall plasser/billetter totalt.
    int  antSolgt;                     //  Antall billetter solgt hittil.
    int  totaltBilletterBestilt;       //  Totalt ›nsket antall billetter.
    int  totaltKunderBestilt;          //  Antall kunder som ›nsker billetter.
    
public:
    Arrangement();
    void oppdater(char titt[], int ant, int pri);
    void oppdaterAntallOnskedeBilletter(int antBill);
    void oppdaterAntallSolgte(int ant);
    bool maaTrekke();
    int  antIgjen();
    int  antBestilt();
    //  Ekstra funksjon for † se hva som skjer ved trekkingen:
    void skriv();
    int checkSex(char n[]);
    int totalPrisKunde(int n);
    void genererFaktura(int nr, ofstream &f);
};

//  GLOBALE VARIABLE:
Kunde        kunder[ANTKUNDER];        //  Alle kundene.
Arrangement  arrangementer[ANTARR];    //  Alle arrangementene.
int          antKunder = 0;                //  Antall kunder som har bestilt.

//  HOVEDPROGRAM:
int main()  {
    
    lesKunder();
    lesArrangementer();
    
    skapFaktura();
    
    return 0;
}

//  DEFINISJON AV KLASSE-FUNKSJONER:
Kunde::Kunde()                         //  Nullstiller arrayers innhold.
{ for (int i = 1;  i < ANTARR;  i++) antBestilt[i] = antFaatt[i] = 0;  }


void Kunde::oppdaterNavn(char nvn[])  //  Oppdaterer kundens navn.
{  strcpy(navn, nvn);  }

//  Oppdaterer element nr."nr" i
//    "antBestilt" med "ant", dersom 'Bestilt'
//    "antFaatt"   med "ant", dersom 'Faatt'
//    "antFaatt"   med "antBestilt[nr]",
//                            dersom 'Kopier':
void Kunde::oppdaterBestiltFaatt(int nr, Oppdater oppdat, int ant)
{  if      (oppdat == Bestilt)  antBestilt[nr] = ant;
else if (oppdat == Faatt)    antFaatt[nr] = ant;
    else                         antFaatt[nr] = antBestilt[nr];  }

//  Returnerer med antall billetter kunden ›nsker p† arrangement nr."nr":
int Kunde::antOnsket(int nr)
{  return  antBestilt[nr];  }

//  Returnerer "true" dersom kunden allerede er
//    tildelt billetter p† arrangement nr."nr":
bool Kunde::faattBillettAllerede(int nr)
{  return (antFaatt[nr] != 0);  }


Arrangement::Arrangement()		//  Nullstiller medlemmene:
{  pris = antPlasser = antSolgt =
    totaltBilletterBestilt = totaltKunderBestilt = 0;  }

//  Oppdaterer en del av medlemmene:
void Arrangement::oppdater(char titt[], int ant, int pri)
{  strcpy(tittel, titt);   antPlasser = ant;   pris = pri;  }

// Oppdaterer hvor mange billetter som er bestilt:
void Arrangement::oppdaterAntallOnskedeBilletter(int antBill)
{  ++totaltKunderBestilt;    totaltBilletterBestilt += antBill;  }

//  Oppdaterer antall solgte billetter:
void Arrangement::oppdaterAntallSolgte(int ant)
{  antSolgt += ant;  }

//  Returnerer "true" dersom ikke alle kundene kan f† sine billett›nsker:
bool Arrangement::maaTrekke()
{  return (antPlasser < totaltBilletterBestilt);  }

//  Returnerer antall ikke solgte:
int Arrangement::antIgjen()
{  return  (antPlasser - antSolgt);  }

//  Returnerer antall kunder bestilt:
int Arrangement::antBestilt()
{  return  totaltKunderBestilt;  }
void Arrangement::skriv() {
}

int Arrangement::checkSex(char n[]) {
    return (int)strlen(n);
}

int Arrangement::totalPrisKunde(int n) {
    
    return pris*n;
    
    return 0;
}

void Arrangement::genererFaktura(int antall, ofstream &f) {
    //cout << "#\n\tKunde: " << nvn << endl;
    
    int totalPris = antall*pris;
    f << "\t#-----------------------------------------------#";
    if(checkSex(tittel) < 32) {
        f << "\n\t# Arrangement: " << tittel << "    #\n";
    }
    else {
        f << "\n\t# Arrangement: " << tittel << " #\n";
    }
    f << "\t#-----------------------------------------------#\n";
    f << "\tAntall: " << setw(7) << antall;
    f << "\tPris: " << setw(5) << pris;
    f << "\tTotalpris: " << setw(5) << totalPris << "\n\n";
    //cout << "\tÅ betale: " << pris << "\n\n";
}



//  DEFINISJONER AV FUNKSJONER:

//  Skriver data om alle arrangementene BIN’RT til filen "BIL_OLXX.RES":
void skrivBilletterTilFil()  {
    int i;
    ofstream utfil("bil_olxx.res", ios::binary);
    for (i = 1;  i < ANTARR;  i++)
        utfil.write( (char *) & arrangementer[i],  sizeof(Arrangement) ) ;
}


//  Skriver data om alle kundene BIN’RT til filen "KUN_OLXX.RES":
void skrivKunderTilFil()  {
    int i;
    ofstream utfil("kun_olxx.res", ios::binary);
    for (i = 1;  i <= antKunder;  i++)     {
        utfil.write( (char *) & kunder[i],  sizeof(Kunde) ) ;
    }
}

void lesKunder() {
    ifstream innfil("kun_olxx.res", ios::binary);
    while(!innfil.eof()) {
        innfil.read((char *) & kunder[++antKunder], sizeof(Kunde));
    }
}

void lesArrangementer() {
    int i = 0;
    ifstream innfil("bil_olxx.res", ios::binary);
    while(!innfil.eof()) {
        innfil.read((char *) & arrangementer[++i], sizeof(Arrangement));
    }
}

void skapFaktura() {
    ofstream faktura("faktura.dat");
    int totalpris = 0;
    for(int i = 1; i < antKunder; i++) {
        faktura << "##############################"
                << "############################\n"
                <<
        "███████╗ █████╗ ██╗  ██╗████████╗██╗   ██╗██████╗  █████╗\n" <<
        "██╔════╝██╔══██╗██║ ██╔╝╚══██╔══╝██║   ██║██╔══██╗██╔══██╗\n" <<
        "█████╗  ███████║█████╔╝    ██║   ██║   ██║██████╔╝███████║\n" <<
        "██╔══╝  ██╔══██║██╔═██╗    ██║   ██║   ██║██╔══██╗██╔══██║\n" <<
        "██║     ██║  ██║██║  ██╗   ██║   ╚██████╔╝██║  ██║██║  ██║\n" <<
        "╚═╝     ╚═╝  ╚═╝╚═╝  ╚═╝   ╚═╝    ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝\n" <<
                   "##############################"
                << "############################\n";
        kunder[i].skriv(faktura);
        for(int j = 1; j < ANTARR; j++) {
            if(kunder[i].antFatt(j) != 0) {
                arrangementer[j].genererFaktura(kunder[i].antFatt(j),
                                                faktura);
                totalpris += arrangementer[j].totalPrisKunde(kunder[i].antFatt(j));
            }
            
        }
        faktura << "\t#-----------------------------------------------#\n";
        faktura << "\tÅ betale:\t" << setw(29) << totalpris << ".00,-\n";
        faktura << "\tKID: " << 10000000 + rand() % 100000 << "\t\t\t\t";
        faktura << "Konto:  " << rand() % 1000
                              << "." << rand() % 100
                              << "." << rand() % 10000
                              << '\n';
        faktura << "\t#-----------------------------------------------#\n";
        faktura << "##############################"
                << "############################\n\n";
        totalpris = 0;
    }
}
